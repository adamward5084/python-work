import os
import pathlib
import re
from math import ceil
from unicodedata import name
from pandas import DataFrame, read_csv
import argparse
from gooey import Gooey, GooeyParser, Events, local_resource_path, types


def split_file_to_csv(file_loc, dest_filename, sep, dlmtr, rowcount, file_ext):
    current_rows = 0
    for i, chunk in enumerate(
        read_csv(file_loc, sep=sep, dtype=object, chunksize=rowcount),
        1
    ):
        print(f"Creating {dest_filename}_{i}{file_ext}")
        chunk.to_csv(
            f"{dest_filename}_{i}{file_ext}",
            sep=dlmtr,
            index=False,
            line_terminator="\n",
        )
        current_rows += rowcount


def split_file_to_excel(file_loc, sep, dest_filename, rowcount):
    current_rows = 0
    for i, chunk in enumerate(
        read_csv(file_loc, sep=sep, dtype=object, chunksize=rowcount),
        1
    ):
        print(f"Creating {dest_filename}_{i}.xlsx")
        chunk.to_excel(f"{dest_filename}_{i}.xlsx", index=False)
        current_rows += rowcount


## Validation


def valid_file_path(filepath : str):
    if pathlib.Path(filepath).exists():
        return str(filepath)
    else:
        raise TypeError(
            f"The path ( {filepath} ) is not a valid filepath. Please try again."
        )

def is_valid_filename(filename : str):
    print(type(filename))
    if type(filename) == str: #re.match('\A(?!(?:COM[0-9]|CON|LPT[0-9]|NUL|PRN|AUX|com[0-9]|con|lpt[0-9]|nul|prn|aux)|\s|[\.]{2,})[^\\\/:*"?<>|]{1,254}(?<![\s\.])\z', filename):
        return filename
    else:
        raise TypeError(
            f"The filename ( {filename} ) is not allowed. Please try again."
        )

def get_dest_file_count(total_rows, chunksize):
    return ceil(total_rows/chunksize)

## Gooey Settings

about_dialog = {
    "type": "HtmlDialog",
    "menuTitle": "About",
    "html": '''
        <center>
        <h2>
        About
        </h2>
        <h3 style='color: blue'>
        Split DAT Files
        </h3>
        <p style='width: 300px; word-wrap: break-word;'>
        This tool was purpose-built for the needs of the Cost Transparency team. It is intended for splitting large, delimited files into smaller, individual files.
        </p>
        <br>
        <ul style='display: inline;'>
        <li>version: 1.2</li>
        <li>Developed by: Adam Ward</li>
        <li>This work is licensed under a Creative Commons Attribution 4.0 International License</li>
        </ul>
        </center>
    ''',
}


@Gooey(
    program_name="Split DAT Files",
    image_dir = local_resource_path("C:\\Users\\warda2\\OneDrive - Blue Cross Blue Shield of Kansas City\\Documents\\SplitClaimsFiles\\images"),
    show_preview_warning=False,
    menu=[{"name": "File", "items": [about_dialog]}],
    use_events=[
        Events.VALIDATE_FORM
    ],
    dump_build_config=False,
    progress_regex=r"^progress: (?P<current_rows>\d+)/(?P<total_rows>\d+)$",
    progress_expr="current_rows / total_rows * 100",
    hide_progress_msg=True,
    timing_options = {
        'show_time_remaining':True,
        'hide_time_remaining_on_complete':True,
    },
    # navigation="Tabbed",
    # tabbed_groups=True,
)
def main():
    current_rows = 0
    parser = GooeyParser(
        description="Divide and Conquer!"
        )
    
    source_grp = parser.add_argument_group(
        "Source File",
        description="Configuration for reading source file.",
        gooey_options={
            "show_border": False,
            "show_underline": True,
            "label_color": "#000000",
            "columns": 1,
        },
    )
    dest_grp = parser.add_argument_group(
        "Destination File(s)",
        description="Configuration for writing the destination files.",
        gooey_options={
            "show_border": False,
            "show_underline": True,
            "label_color": "#000000",
            "columns": 2,
        },
    )

    source_grp.add_argument(
        "--file_loc",
        metavar="Source File Location",
        widget="FileChooser",
        type=valid_file_path,
        help="Which file would you like to be split?",
        gooey_options={
            "full_width": True,
            "wildcard": 
                "DataStage Extract Files (*dat)|*dat|"
                "Comma separated file (*.csv)|*csv|"
                "Excel files (*xlsx)|*xlsx|"
                "All files (*.*)|*.*"
            },
    )

    src_dlmtr_chooser = source_grp.add_mutually_exclusive_group(
        "SourceDelimiter",
        gooey_options={
            "title": "Select Source File Text Delimiter",
            "required": True,
            "initial_value": 0
        },
    )

    src_dlmtr_chooser.add_argument(
        "-p", "--pipe", metavar="Pipe ( | )", const="|", action="store_const"
    )

    src_dlmtr_chooser.add_argument(
        "-m", "--comma", metavar="Comma ( , )", const=",", action="store_const"
    )

    src_dlmtr_chooser.add_argument(
        "-o",
        "--other",
        metavar="Other (Type desired character)",
        type=str,
        action="store",
    )

    dest_dlmtr_chooser = dest_grp.add_mutually_exclusive_group(
        "DestinationDelimiter",
        gooey_options={
            "title": "Select Destination File Text Delimiter",
            "required": True,
            "initial_value": 0,
        },
    )

    dest_dlmtr_chooser.add_argument(
        "--dest_pipe", metavar="Pipe ( | )", const="|", action="store_const"
    )

    dest_dlmtr_chooser.add_argument(
        "--dest_comma", metavar="Comma ( , )", const=",", action="store_const"
    )

    dest_dlmtr_chooser.add_argument(
        "--dest_other",
        metavar="Other (Type desired character)",
        type=str,
        action="store",
    )

    file_type_chooser = dest_grp.add_mutually_exclusive_group(
        "File Type",
        gooey_options={"title": "Select Output File Type", "required": True},
    )

    file_type_chooser.add_argument(
        "-e",
        "--excel",
        metavar="Excel",
        const=".xlsx",
        action="store_const",
        help="Output Excel file (.xlsx)",
    )

    file_type_chooser.add_argument(
        "-c",
        "--csv",
        metavar="CSV",
        const=".csv",
        action="store_const",
        help="Output CSV file (.csv)",
    )

    file_type_chooser.add_argument(
        "-d",
        "--dat",
        metavar="DAT",
        const=".dat",
        action="store_const",
        help="Output DAT file (.dat)",
    )

    dest_grp.add_argument(
        "--rows",
        metavar="Max Rows Per File",
        type=int,
        action="store",
        help="Max rows per file? (Excel row limit is 1,048,576 rows)",
        gooey_options={
            "min": 1000,
            "max": 10000000
        },
    )

    dest_grp.add_argument(
        "--dest_path",
        metavar="Output Folder",
        widget="DirChooser",
        type=valid_file_path,
        help="The desired output directory of the resulting file(s).",
        gooey_options={
            "full_width": True,
            }
    )
    dest_grp.add_argument(
        "--dest_filename",
        metavar="Output Filename",
        type=is_valid_filename,
        help="The desired filename of the resulting file(s). Defaults to original filename.",
    )

    args = parser.parse_args()

    # Get total length of source CSV
    with open(args.file_loc, "r") as f:
        total_rows = sum(1 for row in f)

    source_filepath = args.file_loc

    dest_filename = args.dest_filename or source_filepath.stem
    file_ext = args.dat or args.csv or args.excel
    sep = args.other or args.comma or args.pipe
    dlmtr = args.dest_other or args.dest_comma or args.dest_pipe

    # Change working directory to destination path
    os.chdir(pathlib.Path(args.dest_path))

    if args.excel:
        print(f"Splitting file into {str(get_dest_file_count(total_rows, args.rows))} Excel Files...")
        split_file_to_excel(source_filepath, sep, dest_filename, args.rows)
    elif args.dat or args.csv:
        print(f"Splitting the file into {str(get_dest_file_count(total_rows, args.rows))} {file_ext.upper()[1:]} files...")
        split_file_to_csv(
            source_filepath, dest_filename, sep, dlmtr, args.rows, file_ext
        )
    else:
        raise TypeError(f'Invalid argument given for File Type."')

    print("Done")


if __name__ == "__main__":
    main()
