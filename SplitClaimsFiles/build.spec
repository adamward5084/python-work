# -*- mode: python ; coding: utf-8 -*-

import os
import gooey

gooey_root = os.path.dirname(gooey.__file__)

added_files = [
    ("images", "images")
]

block_cipher = None

a = Analysis(['SplitDATFiles.py'], 
             pathex=['C:\\Users\\warda2\\OneDrive - Blue Cross Blue Shield of Kansas City\\Documents\\SplitClaimsFiles'],
             binaries=[],
             datas=added_files,
             hiddenimports=['pandas', 'gooey', 'argparse', 'openpyxl'],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)

exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='Split DAT Files',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          upx_exclude=[],
          runtime_tmpdir=None,
          console=False,
          disable_windowed_traceback=False,
          argv_emulation=False,
          target_arch=None,
          codesign_identity=None,
          entitlements_file=None,
          icon='images\\program_icon.ico',    
          )
