# Powershell task for clearing out the test DELETEME files once finished with debugging/testing.
$existingVariables = Get-Variable
try {
    # Clear the variables
    $RemoveItems = $false

    while ($RemoveItems -eq $false) {
        
        $DeleteFiles = Read-Host -Prompt 'Do you want to delete the generated files? (Y/n)'

        if ($DeleteFiles -eq 'Y') {
            $UserFilePath = Read-Host -Prompt 'Which directory are they saved in?'
            if ($UserFilePath -ne $false) {
                $FileLoc = Get-ChildItem -Path $UserFilePath
                $RemoveItems = ($FileLoc | Where-Object{$_.Name -Match "^.*DELETEME.*\.(xlsx|csv|dat)"})  
                if ($RemoveItems.Count -gt 0) {
                    Write-Output = ("Removing " + $RemoveItems.Count.ToString() + " DELETEME files from $UserFilePath.")
                    $RemoveItems | Remove-Item
                    Write-Output = "Done"
                    Return
                }
                else {
                    Write-Output = ($RemoveItems.Count.ToString() + " DELETEME files located at $UserFilePath. Please try again")
                }        
            }
        }
        else {
            Return
        }
    }
} finally {
    Get-Variable |
    Where-Object Name -notin $existingVariables.Name |
    Remove-Variable
}
