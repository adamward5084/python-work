import os
import sys
import pathlib
import re
from math import ceil
from typing import List
from unicodedata import name
from pandas import DataFrame, read_csv
import argparse
from gooey import Gooey, GooeyParser, Events, types


def resource_path(relative_path):
    """
    The resource_path function is used to locate data files for the game.
    When running the game from a pyinstaller-generated executable, this function is used to find the location of data files.
    When running the game in other environments (e.g., from source or using a python interpreter), this function returns an empty string.
    
    Args:
        relative_path: Specify the path to a file or folder relative to the location of the current module
    
    Returns:
        The path of the application bundle (
    
    Doc Author:
        Trelent
    """
    
    base_path = getattr(sys, '_MEIPASS', os.path.dirname(os.path.abspath(__file__)))
    return os.path.join(base_path, relative_path)


def split_file_to_csv(
    file_loc, dest_filename, sep, dlmtr, rowcount, total_rows, file_ext
):
    """
    The split_file_to_csv function splits a file into multiple csv files.

    Args:
        file_loc: Specify the location of the file to be split
        dest_filename: Create the name of the file that will be created
        sep: Specify the delimiter used in the csv file
        dlmtr: Specify the delimiter used in the csv file
        rowcount: Determine how many rows will be read from the source file and written to a single csv
        total_rows: Determine the number of rows in the file
        file_ext: Specify the file extension for the destination files
    """
    current_rows = 0
    for i, chunk in enumerate(
        read_csv(file_loc, sep=sep, dtype=object, chunksize=rowcount), 1
    ):
        chunk.to_csv(
            f"{dest_filename}_{i}{file_ext}",
            sep=dlmtr,
            index=False,
            line_terminator="\n",
        )
        current_rows += min(rowcount, (total_rows - current_rows))
        print(f"progress: {current_rows} / {total_rows}")
        sys.stdout.flush()


def split_file_to_excel(file_loc, sep, dest_filename, rowcount, total_rows):
    """
    The split_file_to_excel function splits a file into multiple excel files.
    
    Args:
        file_loc: Specify the file location
        sep: Specify the delimiter used in the csv file
        dest_filename: Specify the name of the excel file that will be created
        rowcount: Determine how many rows to read from the file
        total_rows: Determine when to stop the loop
    """
    current_rows = 0
    for i, chunk in enumerate(
        read_csv(file_loc, sep=sep, dtype=object, chunksize=rowcount), 1
    ):
        chunk.to_excel(f"{dest_filename}_{i}.xlsx", index=False)
        current_rows += min(rowcount, (total_rows - current_rows))
        print(f"progress: {current_rows} / {total_rows}")
        sys.stdout.flush()


## Validation


def valid_file_path(filepath: str):
    """
    The valid_file_path function checks to see if the filepath exists. If it does, then it returns the filepath as a string. Otherwise, an error is raised.
    
    Args:
        filepath:str: Specify the filepath of the file that is to be checked

    Raises:
        TypeError if filepath is not valid
    
    Returns:
        The filepath if the path exists
    """
    if pathlib.Path(filepath).exists():
        return str(filepath)
    else:
        raise TypeError(
            f"The path ( {filepath} ) is not a valid filepath. Please try again."
        )


def is_valid_filename(filename: str):
    """
    The is_valid_filename function checks if the input is a valid filename.
    It checks for invalid characters and ensures that the filename does not start with a space or period.
    The function returns True if it passes all of these tests, otherwise it raises an error.
    
    Args:
        filename:str: Check if the filename is a string
    
    Returns:
        The filename if it is a string and does not contain invalid characters, otherwise it raises a TypeError
    """
    if (
        type(filename) == str
    ):  # re.match('\A(?!(?:COM[0-9]|CON|LPT[0-9]|NUL|PRN|AUX|com[0-9]|con|lpt[0-9]|nul|prn|aux)|\s|[\.]{2,})[^\\\/:*"?<>|]{1,254}(?<![\s\.])\z', filename):
        return filename
    else:
        raise TypeError(
            f"The filename ( {filename} ) is not allowed. Please try again."
        )


def get_dest_file_count(total_rows, chunksize):
    """
    The get_dest_file_count function returns the number of files that will be created by a given
        file_size and total_rows.  This is used to determine how many rows should be written to each
        file.
    
        Args:
            total_rows (int): The number of rows in the source data set.
    
            file_size (int): The size, in bytes, of each destination file.
    
        Returns:
            int: The number of files that will be created by a given source and destination configuration.
    
    Args:
        total_rows: Determine the number of rows in the source file
        chunksize: Specify the number of rows that will be read into memory at a time
    
    Returns:
        The number of files that will be generated by the program
    """
    return ceil(total_rows / chunksize)


def get_user_choice(choices: List) -> str:
    """
    The get_user_choice function returns the value of the choice selected by the user, as indicated by being the first non-falsey value.
    
    Args:
        choices:List: Pass a list of choices to the function
    
    Returns:
        String: First non-falsey value from choices
    """
    return next(x for x in choices if x)


## Gooey Settings

about_dialog = {
    "type": "HtmlDialog",
    "menuTitle": "About",
    "html": """
        <center>
        <h2>
        About
        </h2>
        <h3 style='color: blue'>
        Split DAT Files
        </h3>
        <p style='width: 300px; word-wrap: break-word;'>
        This tool was purpose-built for the needs of the Cost Transparency team. It is intended for splitting large, delimited files into smaller, individual files.
        </p>
        <br>
        <ul style='display: inline;'>
        <li>version: 1.2</li>
        <li>Developed by: Adam Ward</li>
        <li>This work is licensed under a Creative Commons Attribution 4.0 International License</li>
        </ul>
        </center>
    """,
}


@Gooey(
    program_name="Split DAT Files",
    image_dir=resource_path("images"),
    show_preview_warning=False,
    menu=[{"name": "File", "items": [about_dialog]}],
    use_events=[Events.VALIDATE_FORM],
    dump_build_config=False,
    progress_regex=r"^progress: (?P<current>\d+) / (?P<total>\d+)$",
    progress_expr="current / total * 100",
    hide_progress_msg=False,
    # navigation="Tabbed",
    # tabbed_groups=True,
)
def main():
    """
    The main function of this script is to split a large CSV file into smaller files.
    The user can specify the number of rows per file, as well as the output directory and filename.
    
    Args:
    
    Returns:
        The value of the main function
    """
    current_rows = 0
    parser = GooeyParser(description="Divide and Conquer!")

    source_grp = parser.add_argument_group(
        "Source File",
        description="Configuration for reading source file.",
        gooey_options={
            "show_border": False,
            "show_underline": True,
            "label_color": "#000000",
            "columns": 1,
        },
    )
    dest_grp = parser.add_argument_group(
        "Destination File(s)",
        description="Configuration for writing the destination files.",
        gooey_options={
            "show_border": False,
            "show_underline": True,
            "label_color": "#000000",
            "columns": 2,
        },
    )

    source_grp.add_argument(
        "--file_loc",
        metavar="Source File Location",
        widget="FileChooser",
        type=valid_file_path,
        help="Which file would you like to be split?",
        gooey_options={
            "full_width": True,
            "wildcard": "DataStage Extract Files (*dat)|*dat|"
            "Comma separated file (*.csv)|*csv|"
            "Excel files (*xlsx)|*xlsx|"
            "All files (*.*)|*.*",
        },
    )

    src_dlmtr_chooser = source_grp.add_mutually_exclusive_group(
        "SourceDelimiter",
        gooey_options={
            "title": "Select Source File Text Delimiter",
            "required": True,
            "initial_value": 0,
        },
    )

    src_dlmtr_chooser.add_argument(
        "-p", "--pipe", metavar="Pipe ( | )", const="|", action="store_const"
    )

    src_dlmtr_chooser.add_argument(
        "-m", "--comma", metavar="Comma ( , )", const=",", action="store_const"
    )

    src_dlmtr_chooser.add_argument(
        "-o",
        "--other",
        metavar="Other (Type desired character)",
        type=str,
        action="store",
    )

    dest_dlmtr_chooser = dest_grp.add_mutually_exclusive_group(
        "DestinationDelimiter",
        gooey_options={
            "title": "Select Destination File Text Delimiter",
            "required": True,
            "initial_value": 0,
        },
    )

    dest_dlmtr_chooser.add_argument(
        "--dest_pipe", metavar="Pipe ( | )", const="|", action="store_const"
    )

    dest_dlmtr_chooser.add_argument(
        "--dest_comma", metavar="Comma ( , )", const=",", action="store_const"
    )

    dest_dlmtr_chooser.add_argument(
        "--dest_other",
        metavar="Other (Type desired character)",
        type=str,
        action="store",
    )

    file_type_chooser = dest_grp.add_mutually_exclusive_group(
        "File Type",
        gooey_options={"title": "Select Output File Type", "required": True},
    )

    file_type_chooser.add_argument(
        "-e",
        "--excel",
        metavar="Excel",
        const=".xlsx",
        action="store_const",
        help="Output Excel file (.xlsx)",
    )

    file_type_chooser.add_argument(
        "-c",
        "--csv",
        metavar="CSV",
        const=".csv",
        action="store_const",
        help="Output CSV file (.csv)",
    )

    file_type_chooser.add_argument(
        "-d",
        "--dat",
        metavar="DAT",
        const=".dat",
        action="store_const",
        help="Output DAT file (.dat)",
    )

    dest_grp.add_argument(
        "--rows",
        metavar="Max Rows Per File",
        type=int,
        action="store",
        help="Max rows per file? (Excel row limit is 1,048,576 rows)",
        gooey_options={"min": 1000, "max": 10000000},
    )

    dest_grp.add_argument(
        "--dest_path",
        metavar="Output Folder",
        widget="DirChooser",
        type=valid_file_path,
        help="The desired output directory of the resulting file(s).",
        gooey_options={
            "full_width": True,
        },
    )
    dest_grp.add_argument(
        "--dest_filename",
        metavar="Output Filename",
        type=is_valid_filename,
        help="The desired filename of the resulting file(s). Defaults to original filename.",
    )

    args = parser.parse_args()

    # Get total length of source CSV
    with open(args.file_loc, "r") as f:
        total_rows = sum(1 for row in f)

    source_filepath = args.file_loc

    dest_filename = get_user_choice(
        [args.dest_filename, pathlib.Path(source_filepath).stem]
    )
    file_ext = get_user_choice([args.dat or args.csv or args.excel])
    sep = get_user_choice([args.other or args.comma or args.pipe])
    dlmtr = get_user_choice([args.dest_other or args.dest_comma or args.dest_pipe])

    # Change working directory to destination path
    os.chdir(pathlib.Path(args.dest_path))

    if args.excel:
        # print(f"Splitting file into {str(get_dest_file_count(total_rows, args.rows))} Excel Files...")
        split_file_to_excel(source_filepath, sep, dest_filename, args.rows, total_rows)
    elif args.dat or args.csv:
        # print(f"Splitting the file into {str(get_dest_file_count(total_rows, args.rows))} {file_ext.upper()[1:]} files...")
        split_file_to_csv(
            source_filepath, dest_filename, sep, dlmtr, args.rows, total_rows, file_ext
        )
    else:
        raise TypeError(f'Invalid argument given for File Type."')

    print("Done")


if __name__ == "__main__":
    main()
