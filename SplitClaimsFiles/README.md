# Split DAT Files

SplitDatFiles is a Python app for splitting one Excel, CSV, or other delimited file into many smaller Excel or delimited files using a straightforward interface.

Once packaged using the build.spec file and the command below, the executable is completely self-contained and runnable by anyone on any Windows system without any additional resource files.

The user interface is built using [Gooey](https://github.com/chriskiehl/Gooey) and the whole thing was packaged up using [PyInstaller](https://pyinstaller.org/en/stable/). 

Source files are located in source and there's also a util folder with aShell script for cleaning up the files created during testing.

## Development Notes
* Gooey's packaging instructions did not fully work in this case, likely due to being a Windows environment instead of *nix. 

## Building the Final Executable

```bash
> pyinstaller build.spec --upx-dir "<absolute/path/to/upx/dir" --clean
```

## Known Issues
* Radio selections are finicky and sometimes do not fully select, resulting in an error when you hit 'Start'. Going back by clicking 'Edit' and re-selecting them will remedy this.
* Windows Explorer heavily caches application icons so, if you build the executable with a new icon but don't see your changes in Windows Explorer right away, try moving it to another folder to force an update to the icon cache. It will eventually update.

## License
Creative Commons Attribution 4.0 International License