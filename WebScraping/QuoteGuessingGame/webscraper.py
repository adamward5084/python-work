from bs4 import BeautifulSoup
import requests
from pprint import pprint
from dataclasses import dataclass
from typing import Dict, List
import random


base_url = "http://quotes.toscrape.com"


@dataclass
class QuoteDetail:
    quote: str
    author: str
    bio_url: str
    hints: List


@dataclass
class QuoteRepo:
    quotes: List[QuoteDetail]


def get_html(url_str: str) -> str:
    response = requests.get(url_str)
    if response.status_code == 200:
        return response.text
    else:
        return "None"


def soupify(html: str):
    return BeautifulSoup(html, "html.parser")


def get_quote_info():
    try:
        page_url = "/page/1/"
        quote_list = list()
        author_list = list()
        url_list = list()
        while page_url:
            html = get_html(base_url + page_url)
            soup = soupify(html)
            quote_list.extend([q.getText() for q in soup.find_all(class_="text")])
            author_list.extend([q.getText() for q in soup.find_all(class_="author")])
            url_list.extend(
                [
                    q.findChildren("a", recursive=True)[0]["href"]
                    for q in soup.find_all(class_="quote")
                ]
            )
            page_url = (
                soup.find(class_="next").findChildren("a", recursive=False)[0]["href"]
                if soup.find(class_="next")
                else None
            )
        return quote_list, author_list, url_list
    except Exception as e:
        print(e)
        print(page_url)


def make_database() -> QuoteRepo:
    print("Building quote database...")
    quote_list, author_list, url_list = get_quote_info()
    if len(quote_list) == len(author_list) == len(url_list):
        quote_data = QuoteRepo(quotes=list())
        for i in range(len(quote_list)):
            quote_data.quotes.append(
                QuoteDetail(
                    quote=quote_list[i],
                    author=author_list[i],
                    bio_url=url_list[i],
                    hints=[],
                )
            )
    return quote_data


def populate_hints(quote):
    try:
        full_url = base_url + quote.bio_url
        html = get_html(full_url)
        bio_soup = soupify(html)
        whole_bio = bio_soup.find(class_="author-description").getText()
        name = quote.author.split(" ")
        for i in range(len(name)):
            whole_bio = whole_bio.replace(name[i], "[BLANK]")
        quote.hints.extend(
            [
                f"Born {bio_soup.find(class_='author-born-location').getText()} on {bio_soup.find(class_='author-born-date').getText()}."
            ]
        )
        split_bio = whole_bio.split(". ")
        quote.hints.extend([sentence for sentence in split_bio])
    except Exception as e:
        print(e)
        print(quote.author, quote.bio_url)


def main():
    print("Welcome to Guess That Quote!")
    print("Please standby while we start the game...")
    keep_playing = "y"
    quote_db = make_database()
    while keep_playing == "y":
        round_quote = random.choice(quote_db.quotes)
        populate_hints(round_quote)
        print("Guess who said the following quote:\n\n")
        print(round_quote.quote)
        print("\n")
        tries_remaining = 4
        answer = input("Answer (case-sensitive): ")
        while tries_remaining > 0 and answer.lower() != round_quote.author.lower():
            if answer.lower() == round_quote.author.lower():
                print("Bingo! You got it!")
                break
            print("Incorrect. Here's a hint:\n")
            print(random.choice(round_quote.hints))
            answer = input("Answer (case-sensitive): ")
            tries_remaining -= 1
        print(f"The answer was {round_quote.author}.")
        keep_playing = input("Would you like to keep playing? (y/n): ")
    print("Thanks for playing!")


if __name__ == "__main__":
    main()
