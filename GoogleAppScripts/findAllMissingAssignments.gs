function listAllAssignments() {
  var ss = SpreadsheetApp.getActiveSpreadsheet();  
  var response = Classroom.Courses.list();
  var courses = response.courses;
  for (var i = 0; i < courses.length; i++) {
    // Arrange Classwork
    var missing_work=[];
    var course = courses[i];
    var classwork = Classroom.Courses.CourseWork.list(course.id);
    var all_homework = classwork.courseWork;

    // Arrange Spreadsheet
    var tab = ss.getSheetByName(course.name)
    if (!tab) {
      var tab = ss.insertSheet().setName(course.name)
    }

    // Find homework for current course
    for (var h = 0; h < all_homework.length; h++) {
      var course_homework = all_homework[h];
      var submission = Classroom.Courses.CourseWork.StudentSubmissions.list(
        course_homework.courseId, course_homework.id
        ).studentSubmissions[0]
      if(submission.state != "TURNED_IN"){
        if (course_homework.dueDate){
          var duedate = dateFormatter(course_homework.dueDate);
        }
        if(duedate > Date.parse('01 Jan 2023 00:00:00 CST') || !duedate){
          var assignment_title = course_homework.title;
          var status = submission.late ? "LATE" : "Due";
          missing_work.push( 
            [
              assignment_title,
              new Date(Date.parse(course_homework.creationTime)),
              duedate,
              course_homework.alternateLink,
              status,
            ]
          );
        };
      }
    }
    console.log(missing_work.length + " assignments found for " + course.name + ".")
    if(missing_work[0]){
      tab.getRange(1,1,1,missing_work[0].length).setValues([["Assignment", "Created", "DueDate", "Link", "Status"]])
      tab.getRange(2, 1, missing_work.length, missing_work[0].length).setValues(missing_work).sort(3);   
      ss.setActiveSheet(tab)
      formatColumnHeader()
      tab.autoResizeColumns(1,missing_work[0].length)
    }
    else {
      ss.setActiveSheet(tab)
      ss.deleteActiveSheet()
    }
  }
};


function dateFormatter(date_val){
  const result = new Date()
  result.setMonth(date_val.month-1, date_val.day)
  result.setFullYear(date_val.year)
  return result
};


/**
 * Formats the column header of the active sheet.
 */ 
function formatColumnHeader() {  
  var sheet = SpreadsheetApp.getActiveSheet();
  sheet.clearFormats()
  // Get total number of rows in data range, not including
  // the header row.
  var numRows = sheet.getDataRange().getLastRow() - 1;
  
  // Get the range of the column header.
  var columnHeaderRange = sheet.getRange(2, 1, numRows, 1);
  
  // Apply text formatting and add borders.
  columnHeaderRange
    .setFontWeight('bold')
    .setFontStyle('italic')
    .setBorder(
      true, true, true, true, null, null,
      null,
      SpreadsheetApp.BorderStyle.SOLID_MEDIUM); 
  sheet.setFrozenRows(1);

  // Call helper method to hyperlink the first column contents
  // to the url column contents.
  hyperlinkColumnHeaders_(columnHeaderRange, numRows); 

  /**
   * Helper function that hyperlinks the column header with the
   * 'url' column contents. The function then removes the column.
   *
   * @param {object} headerRange The range of the column header
   *   to update.
   * @param {number} numRows The size of the column header.
   */
  function hyperlinkColumnHeaders_(headerRange, numRows) {
    // Get header and url column indices.
    var headerColIndex = 1; 
    var urlColIndex = columnIndexOf_('Link');  
    
    // Exit if the url column is missing.
    if(urlColIndex == -1)
      return; 
    
    // Get header and url cell values.
    var urlRange =
      headerRange.offset(0, urlColIndex - headerColIndex);
    var headerValues = headerRange.getValues();
    var urlValues = urlRange.getValues();
    
    // Updates header values to the hyperlinked header values.
    for(var row = 0; row < numRows; row++){
      headerValues[row][0] = '=HYPERLINK("' + urlValues[row]
        + '","' + headerValues[row] + '")';
    }
    headerRange.setValues(headerValues);
    
    // Delete the url column to clean up the sheet.
    SpreadsheetApp.getActiveSheet().deleteColumn(urlColIndex);
    };
  /**
   * Helper function that goes through the headers of all columns
   * and returns the index of the column with the specified name
   * in row 1. If a column with that name does not exist,
   * this function returns -1. If multiple columns have the same
   * name in row 1, the index of the first one discovered is
   * returned.
   * 
   * @param {string} colName The name to find in the column
   *   headers. 
   * @return The index of that column in the active sheet,
   *   or -1 if the name isn't found.
   */ 
  function columnIndexOf_(colName) {
    // Get the current column names.
    var sheet = SpreadsheetApp.getActiveSheet();
    var columnHeaders =
      sheet.getRange(1, 1, 1, sheet.getLastColumn());
    var columnNames = columnHeaders.getValues();
    
    // Loops through every column and returns the column index
    // if the row 1 value of that column matches colName.
    for(var col = 1; col <= columnNames[0].length; col++)
    {
      if(columnNames[0][col-1] === colName)
        return col; 
    }

    // Returns -1 if a column named colName does not exist. 
    return -1; 
  }
};